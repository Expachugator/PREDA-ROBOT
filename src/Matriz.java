/*
 *  Crea matrices, las imprime en pantalla y exporta a archivo de salida
 */

public class Matriz {

	public static Nodo[][] crearMatriz(int n, int m, String[] entrada) {

		Nodo[][] matriz = new Nodo[n][m];
		int pos = 0;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {

				Nodo nodo = new Nodo(pos + 1, entrada[pos], j, i);
				matriz[i][j] = nodo;
				pos++;
			}
		}
		return matriz;
	}

	public static void imprimirMatriz(int n, int m, Nodo[][] matriz) {

		for (int a = 0; a < n; a++) {
			for (int b = 0; b < m; b++) {
				Nodo nodo = new Nodo();
				nodo = matriz[a][b];
				String valor = nodo.getPeso();
				System.out.print(valor);
				System.out.print("  ");
			}
			System.out.println("");
		}
	}

	public static void escribirMatriz(int n, int m, Nodo[][] matriz) {

		for (int a = 0; a < n; a++) {
			Main.trazaArch = Main.trazaArch + "	";
			for (int b = 0; b < m; b++) {
				Nodo nodo = new Nodo();
				nodo = matriz[a][b];
				String valor = nodo.getPeso();
				Main.trazaArch = Main.trazaArch + valor;
				Main.trazaArch = Main.trazaArch + "  ";
			}
			Main.trazaArch = Main.trazaArch + "\r\n";
		}
	}
}