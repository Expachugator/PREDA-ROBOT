import java.io.*;
import java.util.*;

/*
 * 	Recoje el archivo de entrada, se procesa y se aplica el algoritmo de Dijkstra
 *  para ir desde el punto R hasta el punto S utilizando el camino mas optimo
 * 
 */

public class Main {

	public static String trazaArch;

	public static void main(String[] args) throws IOException {

		long tInicio = System.currentTimeMillis();

		try {
			getArgumentos(args, tInicio);
		} catch (IOException | IllegalStateException | NumberFormatException e) {
			System.out.println(e);
		}
	}

	/*
	 * Se recogen los arggumentos y se tratan segun sean sus valores
	 */
	private static void getArgumentos(String[] args, long tInicio) throws IOException {
		try {
			if (args.length == 0) {
				System.out.println("No se han detectado parametros, no se puede continuar");
			} else if (args.length == 1) {
				if (args[0].equals("-h")) {
					ayuda();
				} else {
					resultadoPantalla(args[0], tInicio);
				}
			} else if (args.length == 2) {
				if (args[0].equals("-t")) {
					resultadoTrazasPantalla(args[1], tInicio);
				} else {
					resultadoArchivo(args[0], args[1], tInicio);
				}
			} else if (args.length == 3) {
				resultadoTrazasArchivo(args[1], args[2], tInicio);
			}
		} catch (IllegalStateException | NumberFormatException e) {
			System.out.println(e);
		}
	}

	/*
	 * Imprime la ayuda en pantalla
	 */
	private static void ayuda() {
		System.out.println("SINTAXIS:");
		System.out.println("robot [-t] [-h] [fichero_entrada] [fichero_salida]");
		System.out.println("-t                      Traza la aplicacion del algoritmo a los datos");
		System.out.println("-h                      Muestra esta ayuda");
		System.out.println("fichero_entrada         Nombre del fichero de entrada");
		System.out.println("fichero_salida          Nombre del fichero de salida");
	}

	/*
	 * Imprime el resultado en pantalla
	 */
	private static void resultadoPantalla(String args0, long tInicio) {
		String fichero = args0;
		String salida = null;
		ejecuccion(fichero, false, salida, tInicio);
	}

	/*
	 * Imprime el resultado en pantalla con trazas
	 */
	private static void resultadoTrazasPantalla(String args1, long tInicio) {
		String fichero = args1;
		String salida = null;
		ejecuccion(fichero, true, salida, tInicio);
	}

	/*
	 * Exporta el resultado a un archivo
	 */
	private static void resultadoArchivo(String args0, String args1, long tInicio) {
		String fichero = args0;
		String salida = args1;
		ejecuccion(fichero, false, salida, tInicio);
	}

	/*
	 * Exporta el resultado a un archivo con trazas
	 */
	private static void resultadoTrazasArchivo(String args1, String args2, long tInicio) {
		String fichero = args1;
		String salida = args2;
		ejecuccion(fichero, true, salida, tInicio);
	}

	/*
	 * Comprueba que los datos de entrada son correctos y ejecuta el algoritmo
	 */
	private static void ejecuccion(String fichero, boolean trazas, String salida, long tInicio) {

		try {
			boolean comprobacionesEntrada;
			comprobacionesEntrada = ComprobacionesEntrada.comprobacionesEntrada(fichero);

			if (comprobacionesEntrada) {

				BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fichero), "UTF-8"));

				int n = Integer.parseInt(in.readLine());
				int m = Integer.parseInt(in.readLine());
				int tamano = n * m;
				String[] entrada = new String[tamano];
				String cadena;
				int i = 0;
				while ((cadena = in.readLine()) != null) {
					entrada[i] = cadena;
					i++;
				}
				in.close();

				if (salida != null) {
					trazaArch = "Matriz: " + n + "x" + m + "\r\n\r\n";
				} else {
					System.out.println("Matriz: " + n + "x" + m);
				}
				Nodo[][] matriz = Matriz.crearMatriz(n, m, entrada);
				// System.out.println("\nCircuito:\n");
				// Matriz.imprimirMatriz(n, m, matriz);
				Grafo grafo = new Grafo(n, m, matriz);
				if (trazas) {
					if (salida != null) {
						trazaArch = trazaArch + "\n\nCircuito:\r\n";
						Matriz.escribirMatriz(n, m, matriz);
						Dijkstra dijkstra = new Dijkstra(tamano, grafo.getListaNodos(), trazas, salida);
						dijkstra.escribirSalida();
						tiempo(tInicio, salida);
					} else {
						System.out.println("\nCircuito:\n");
						Matriz.imprimirMatriz(n, m, matriz);
						System.out.println("\nLista de nodos:");
						Iterator<Nodo> it = grafo.getListaNodos().iterator();
						while (it.hasNext()) {
							Nodo nodo = it.next();
							System.out.println("	Numero: " + nodo.getNumero());
							System.out.println("	Posicion: " + nodo.getPosY() + nodo.getPosX());
							System.out.println("	Valor: " + nodo.getPeso());
							System.out.print("	Nodos adyacentes: ");
							Iterator<Nodo> lista = nodo.getListaAdyacente().iterator();
							while (lista.hasNext()) {
								Nodo adya = lista.next();
								System.out.print(adya.getNumero());
								if (lista.hasNext()) {
									System.out.print(", ");
								}
							}
							System.out.println("\n");
						}
						Dijkstra dijkstra = new Dijkstra(tamano, grafo.getListaNodos(), trazas, salida);
						dijkstra.pintarSalida();
						tiempo(tInicio, salida);
					}
				} else {
					if (salida != null) {
						Dijkstra dijkstra = new Dijkstra(tamano, grafo.getListaNodos(), trazas, salida);
						dijkstra.escribirSalida();
						tiempo(tInicio, salida);
					} else {
						Dijkstra dijkstra = new Dijkstra(tamano, grafo.getListaNodos(), trazas, salida);
						dijkstra.pintarSalida();
						tiempo(tInicio, salida);
					}
				}
			}
			if (salida != null) {
				File archivo = new File(salida);
				BufferedWriter bw = new BufferedWriter(new FileWriter(archivo));
				bw.write(trazaArch);
				bw.close();
				System.out.println("Creado archivo de salida en " + archivo);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Indica el tiempo total de ejecucion del programa
	 */
	private static void tiempo(long tInicio, String salida) {
		if (salida != null) {
			long tFin = System.currentTimeMillis();
			long tTotal = tFin - tInicio;
			trazaArch = trazaArch + "\r\n";
			trazaArch = trazaArch + "Tiempo total: " + tTotal + "ms";
		} else {
			long tFin = System.currentTimeMillis();
			long tTotal = tFin - tInicio;
			System.out.println();
			System.out.println("Tiempo total: " + tTotal + "ms");
		}
	}
}