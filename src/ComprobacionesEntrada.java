import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 *  Ejecuta las comprobaciones del fichero de entrada pertinentes
 */

public class ComprobacionesEntrada {

	public static boolean comprobacionesEntrada(String fichero) throws IOException {

		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fichero), "UTF-8"));
		boolean tamano = comprobacionTamano(in);
		in = new BufferedReader(new InputStreamReader(new FileInputStream(fichero), "UTF-8"));
		boolean valores = comprobacionValores(in);
		in = new BufferedReader(new InputStreamReader(new FileInputStream(fichero), "UTF-8"));
		boolean numEnSalObs = comprobacionNumeroSalidasRobotObstaculos(in);
		in = new BufferedReader(new InputStreamReader(new FileInputStream(fichero), "UTF-8"));
		boolean ubiSal = comprobacionUbicacionSalida(in);

		if ((tamano && valores && numEnSalObs && ubiSal) == false) {
			System.out.println("Entrada incorrecta, no se puede continuar");
			return false;
		}
		System.out.println("Entrada correcta, continuando ejecucion");
		return true;
	}

	private static boolean comprobacionTamano(BufferedReader in) throws IOException {

		String lineaN = in.readLine();
		String lineaM = in.readLine();
		boolean valorN = true;
		boolean valorM = true;

		if (lineaN.matches("[^\\d*]")) {
			System.out.println("Error: Valor de n incorrecto");
			valorN = false;
			in.close();
			return false;
		}
		if (lineaM.matches("[^\\d*]")) {
			System.out.println("Error: Valor de m incorrecto");
			valorM = false;
			in.close();
			return false;
		}

		if (valorN == false || valorM == false) {
			in.close();
			return false;
		} else {
			int i = 0;
			int n = Integer.parseInt(lineaN);
			int m = Integer.parseInt(lineaM);
			while ((in.readLine()) != null) {
				i++;
			}
			if (n * m == i) {
				System.out.println("Numero de entradas correcto");
				in.close();
				return true;
			} else {
				System.out.println("Error: Numero de entradas incorrecto");
				in.close();
				return false;
			}
		}
	}

	private static boolean comprobacionValores(BufferedReader in) throws IOException {

		String cadena;
		long valor;
		while ((cadena = in.readLine()) != null) {
			if (cadena.matches("\\d*")) {
				valor = Long.parseLong(cadena);
				if (valor > Integer.MAX_VALUE) {
					in.close();
					System.out.println("Error: Se han encontrado valores demasiado grande");
					in.close();
					return false;
				}
				if (valor < 1) {
					in.close();
					System.out.println("Error: Se han encontrado valores inferiores a 1");
					in.close();
					return false;
				}
			} else if (!cadena.equals("R") && !cadena.equals("S") && !cadena.equals("O")) {
				in.close();
				System.out.println("Error: caracter no numerico distinto de R, S, O, valor incorrecto");
				in.close();
				return false;
			}
		}
		in.close();
		System.out.println("Valores correctos");
		return true;
	}

	private static boolean comprobacionNumeroSalidasRobotObstaculos(BufferedReader in) throws IOException {
		int numR = 0;
		int numS = 0;
		int numO = 0;
		String cadena;
		while ((cadena = in.readLine()) != null) {
			if (cadena.matches("O")) {
				numO++;
			} else if (cadena.matches("S")) {
				numS++;
			} else if (cadena.matches("R")) {
				numR++;
			}
		}
		in.close();
		if (numR < 1 || numR > 1) {
			System.out.println("Numero de robots incorrecto");
		}
		if (numS < 1 || numS > 1) {
			System.out.println("Numero de salidas incorrecto");
		}
		if (numO < 1) {
			System.out.println("Numero de obstaculos incorrecto");
		}
		if (numR > 1 || numS > 1 || numO < 1) {
			return false;
		}
		System.out.println("Numero de robots, salidas y obstaculos correcto");
		return true;
	}

	private static boolean comprobacionUbicacionSalida(BufferedReader in) throws IOException {

		String cadena = in.readLine();
		int n = Integer.parseInt(cadena);
		cadena = in.readLine();
		int m = Integer.parseInt(cadena);
		int pos = 1;
		int aux;

		while ((cadena = in.readLine()) != null && !cadena.matches("S")) {
			if (!cadena.matches("S")) {
				pos++;
			}
		}

		if (pos <= m) {
			System.out.println("Posicion de S correcta");
			return true;
		}

		for (int i = 1; i < n; i++) {
			aux = i * m;
			if (aux == pos || aux + 1 == pos) {
				System.out.println("Posicion de S correcta");
				return true;
			}
		}
		aux = (n - 1) * m;

		if (pos > aux) {
			System.out.println("Posicion de S correcta");
			return true;
		}
		System.out.println("Posicion de S incorrecta");
		return false;
	}
}