import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

/*
 *  Se crea y ejecuta el algoritmo de Dijkstra
 */

public class Dijkstra {

	private int[] especial;
	private int energia = 0;
	private List<Nodo> camino;

	public Dijkstra(int tamano, List<Nodo> listaNodos, boolean trazas, String salida) {
		boolean nodoSenC = true;
		int pasos = 0;

		especial = new int[tamano];
		predecesor = new int[tamano];

		List<Nodo> s = new ArrayList<Nodo>();
		List<Nodo> c = new ArrayList<Nodo>();
		Iterator<Nodo> it = listaNodos.iterator();
		int aux = 0;
		int posRobot = 0;
		while (it.hasNext()) {
			Nodo nodo = it.next();
			if (nodo.getPeso().equals("R")) {
				s.add(nodo);
				posRobot = aux;
			} else {
				c.add(nodo);
			}
			aux++;
		}

		Iterator<Nodo> itEsp = listaNodos.iterator();
		PriorityQueue<Nodo> priorityQueue = new PriorityQueue<Nodo>();
		while (itEsp.hasNext()) {
			Nodo nodo = itEsp.next();
			especial[nodo.getNumero() - 1] = distancia(s.get(0), nodo);
			if (especial[nodo.getNumero() - 1] != Integer.MAX_VALUE) {
				nodo.setValorEspecial(especial[nodo.getNumero() - 1]);
				priorityQueue.add(nodo);
			}
			predecesor[nodo.getNumero() - 1] = s.get(0).getNumero();
		}
		while (nodoSenC == true) {
			Nodo v = priorityQueue.poll();
			if (trazas) {
				if (salida != null) {
					trazasArchivo1(pasos, s, v);
					pasos++;
				} else {
					trazasPantalla1(pasos, s, v);
					pasos++;
				}
			}
			c.remove(v);

			if (v != null && !v.getPeso().equals("S")) {
				Iterator<Nodo> itC = c.iterator();
				while (itC.hasNext()) {
					Nodo nodo = itC.next();
					int sumaEspDist = sumaEspDist(especial[v.getNumero() - 1], distancia(v, nodo));
					if (especial[nodo.getNumero() - 1] > sumaEspDist) {
						especial[nodo.getNumero() - 1] = sumaEspDist;
						nodo.setValorEspecial(sumaEspDist);
						priorityQueue.add(nodo);
						predecesor[nodo.getNumero() - 1] = v.getNumero();
					}
				}
			} else {
				nodoSenC = false;
			}
			if (trazas) {
				if (salida != null) {
					trazasArchivo2(posRobot, especial, predecesor);
				} else {
					trazasPantalla2(posRobot, especial, predecesor);
				}
			}
		}
		camino = calcularCamino(especial, predecesor, listaNodos);
	}

	private int[] predecesor;

	private int distancia(Nodo origen, Nodo destino) {
		Iterator<Nodo> it = origen.getListaAdyacente().iterator();
		while (it.hasNext()) {
			Nodo i = it.next();
			if (i.getNumero() == destino.getNumero()) {
				if (destino.getPeso().matches("[^\\d*]")) {
					if (destino.getPeso().matches("O")) {
						return Integer.MAX_VALUE;
					} else if (destino.getPeso().matches("S")) {
						return 0;
					}
				} else {
					return Integer.parseInt(destino.getPeso());
				}
			}
		}
		return Integer.MAX_VALUE;
	}

	private List<Nodo> calcularCamino(int[] especial, int[] predecesor, List<Nodo> listaNodos) {

		List<Nodo> camino = new ArrayList<Nodo>();
		boolean fin = false;
		int buscarPred = 0;
		Nodo nodoAux = null;
		int i = 0;
		Iterator<Nodo> it = listaNodos.iterator();
		while (it.hasNext()) {
			Nodo nodo = it.next();
			if (nodo.getPeso().equals("S")) {
				buscarPred = nodo.getNumero();
				nodoAux = nodo;
				camino.add(nodoAux);
				energia = especial[i];
			}
			i++;
		}

		while (fin == false) {
			buscarPred = predecesor[nodoAux.getNumero() - 1];
			nodoAux = listaNodos.get(buscarPred - 1);
			camino.add(0, nodoAux);
			if (nodoAux.getPeso().equals("R")) {
				fin = true;
			}
		}
		return camino;
	}

	private int sumaEspDist(int a, int b) {
		if (a == Integer.MAX_VALUE || b == Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		} else {
			return a + b;
		}
	}

	public void pintarSalida() {

		if (energia == Integer.MAX_VALUE) {
			System.out.println("\nEl robot no puede alcanzar la salida");
		} else {
			Iterator<Nodo> it = camino.iterator();
			Nodo nodo = new Nodo();
			int x, y;
			System.out.println();
			System.out.print("R");
			while (it.hasNext()) {
				nodo = it.next();
				x = nodo.getPosX() + 1;
				y = nodo.getPosY() + 1;
				if (it.hasNext()) {
					System.out.print("[" + y + "," + x + "],");
				} else {
					System.out.print("S[" + y + "," + x + "]");
				}
			}
			System.out.println();
			System.out.println("Energia total consumida: " + energia);
		}
	}

	public void escribirSalida() {

		if (energia == Integer.MAX_VALUE) {
			Main.trazaArch = Main.trazaArch + "\r\nEl robot no puede alcanzar la salida";
		} else {
			Iterator<Nodo> it = camino.iterator();
			Nodo nodo = new Nodo();
			int x, y;
			Main.trazaArch = Main.trazaArch + "\r\nR\r\n";
			while (it.hasNext()) {
				nodo = it.next();
				x = nodo.getPosX() + 1;
				y = nodo.getPosY() + 1;
				if (it.hasNext()) {
					Main.trazaArch = Main.trazaArch + "[" + y + "," + x + "],";
				} else {
					Main.trazaArch = Main.trazaArch + "S[" + y + "," + x + "]";
				}
			}
			Main.trazaArch = Main.trazaArch + "\r\nEnergia total consumida: " + energia + "\r\n";
		}
	}

	private void trazasPantalla1(int pasos, List<Nodo> c, Nodo v) {
		System.out.println("\nPASO: " + pasos);
		if (v == null) {
			System.out.print("No hay salida, finalizando ejecucion");
		} else {
			System.out.println("V seleccionado: " + v.getNumero());
			System.out.println("C:");
			Iterator<Nodo> auxC = c.iterator();
			while (auxC.hasNext()) {
				Nodo nodo = auxC.next();
				System.out.print(nodo.getNumero() + " ");
			}
		}
	}

	private void trazasPantalla2(int posRobot, int[] especial, int[] predecesor) {
		System.out.println("\nEspecial:");
		for (int i = 0; i < especial.length; i++) {
			if (i == posRobot) {
				System.out.print("- ");
			} else if (especial[i] == Integer.MAX_VALUE) {
				System.out.print("\u221E ");
			} else {
				System.out.print(especial[i] + " ");
			}
		}
		System.out.println("\nPredecesor: ");
		for (int i = 0; i < predecesor.length; i++) {
			if (predecesor[i] == Integer.MAX_VALUE) {
				System.out.print("\u221E ");
			} else {
				System.out.print(predecesor[i] + " ");
			}
		}
		System.out.println("");
	}

	private void trazasArchivo1(int pasos, List<Nodo> c, Nodo v) {
		Main.trazaArch = Main.trazaArch + "PASO: " + pasos + "\r\n";
		if (v == null) {
			Main.trazaArch = Main.trazaArch + "No hay salida, finalizando ejecucion";
		} else {
			Main.trazaArch = Main.trazaArch + "V seleccionado: " + v.getNumero() + "\r\n";
			Main.trazaArch = Main.trazaArch + "C:\r\n";
			Iterator<Nodo> auxC = c.iterator();
			while (auxC.hasNext()) {
				Nodo nodo = auxC.next();
				Main.trazaArch = Main.trazaArch + nodo.getNumero() + " ";
			}
		}
	}

	private void trazasArchivo2(int posRobot, int[] especial, int[] predecesor) {
		Main.trazaArch = Main.trazaArch + "\r\nEspecial:";
		for (int i = 0; i < especial.length; i++) {
			if (i == posRobot) {
				Main.trazaArch = Main.trazaArch + "- ";
			} else if (especial[i] == Integer.MAX_VALUE) {
				Main.trazaArch = Main.trazaArch + "\u221E ";
			} else {
				Main.trazaArch = Main.trazaArch + especial[i] + " ";
			}
		}
		Main.trazaArch = Main.trazaArch + "\r\nPredecesor: \r\n";
		for (int i = 0; i < predecesor.length; i++) {
			if (predecesor[i] == Integer.MAX_VALUE) {
				Main.trazaArch = Main.trazaArch + "\u221E ";
			} else {
				Main.trazaArch = Main.trazaArch + predecesor[i] + " ";
			}
		}
		Main.trazaArch = Main.trazaArch + "\r\n\r\n";
	}
}