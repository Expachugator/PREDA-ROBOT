import java.util.ArrayList;
import java.util.List;

/*
 *  Crea los nodos con sus atributos y contiene los getters y setters
 */

public class Nodo implements Comparable<Nodo> {
	private String peso;
	private int posX;
	private int posY;
	private List<Nodo> listaAdyacente = new ArrayList<Nodo>();
	private int numero;
	private int valorEspecial;

	public Nodo() {
	}

	public Nodo(int numero, String peso, int posX, int posY) {
		this.numero = numero;
		this.peso = peso;
		this.posX = posX;
		this.posY = posY;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public List<Nodo> getListaAdyacente() {
		return listaAdyacente;
	}

	public void setListaAdyacente(int posX, int posY, Nodo[][] matriz) {
		Nodo nodo = matriz[posY][posX]; // Para adya
		listaAdyacente.add(nodo);
	}

	public int getValorEspecial() {
		return valorEspecial;
	}

	public void setValorEspecial(int valorEspecial) {
		this.valorEspecial = valorEspecial;
	}

	@Override
	public int compareTo(Nodo o) {
		if (this.valorEspecial == o.valorEspecial) {
			return 0;
		} else if (this.valorEspecial > o.valorEspecial) {
			return 1;
		} else {
			return -1;
		}
	}
}