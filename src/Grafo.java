import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
 *  Crea el grafo con sus nodos
 */

public class Grafo {

	private static List<Nodo> listaNodos = new ArrayList<Nodo>();

	public Grafo(int n, int m, Nodo[][] matriz) {
		anadirNodosLista(n, m, matriz);
		crearListaAdya(n, m, matriz);
	}

	private void crearListaAdya(int n, int m, Nodo[][] matriz) {
		Iterator<Nodo> it = listaNodos.iterator();
		while (it.hasNext()) {
			Nodo i = it.next();
			if (m > 1) {
				if (n > 1) {
					if (i.getPosY() == 0) {
						if (i.getPosX() == 0) {
							i.setListaAdyacente(1, 0, matriz);
							i.setListaAdyacente(0, 1, matriz);
							i.setListaAdyacente(1, 1, matriz);
						} else if (i.getPosX() + 1 < m) {
							i.setListaAdyacente(i.getPosX() - 1, 0, matriz);
							i.setListaAdyacente(i.getPosX() + 1, 0, matriz);
							i.setListaAdyacente(i.getPosX() - 1, 1, matriz);
							i.setListaAdyacente(i.getPosX(), 1, matriz);
							i.setListaAdyacente(i.getPosX() + 1, 1, matriz);
						} else {
							i.setListaAdyacente(i.getPosX() - 1, 0, matriz);
							i.setListaAdyacente(i.getPosX() - 1, 1, matriz);
							i.setListaAdyacente(i.getPosX(), 1, matriz);
						}
					} else if (i.getPosY() + 1 == n) {
						if (i.getPosX() == 0) {
							i.setListaAdyacente(0, i.getPosY() - 1, matriz);
							i.setListaAdyacente(1, i.getPosY() - 1, matriz);
							i.setListaAdyacente(1, i.getPosY(), matriz);
						} else if (i.getPosX() + 1 < m) {
							i.setListaAdyacente(i.getPosX() - 1, n - 1, matriz);
							i.setListaAdyacente(i.getPosX() - 1, n - 2, matriz);
							i.setListaAdyacente(i.getPosX(), n - 2, matriz);
							i.setListaAdyacente(i.getPosX() + 1, n - 2, matriz);
							i.setListaAdyacente(i.getPosX() + 1, n - 1, matriz);
						} else {
							i.setListaAdyacente(m - 2, n - 1, matriz);
							i.setListaAdyacente(m - 2, n - 2, matriz);
							i.setListaAdyacente(m - 1, n - 2, matriz);
						}
					} else {
						if (i.getPosX() == 0) {
							i.setListaAdyacente(0, i.getPosY() - 1, matriz);
							i.setListaAdyacente(1, i.getPosY() - 1, matriz);
							i.setListaAdyacente(1, i.getPosY(), matriz);
							i.setListaAdyacente(1, i.getPosY() + 1, matriz);
							i.setListaAdyacente(0, i.getPosY() + 1, matriz);
						} else if (i.getPosX() + 1 == m) {
							i.setListaAdyacente(m - 1, i.getPosY() - 1, matriz);
							i.setListaAdyacente(m - 2, i.getPosY() - 1, matriz);
							i.setListaAdyacente(m - 2, i.getPosY(), matriz);
							i.setListaAdyacente(m - 2, i.getPosY() + 1, matriz);
							i.setListaAdyacente(m - 1, i.getPosY() + 1, matriz);
						} else {
							i.setListaAdyacente(i.getPosX() - 1, i.getPosY() - 1, matriz);
							i.setListaAdyacente(i.getPosX(), i.getPosY() - 1, matriz);
							i.setListaAdyacente(i.getPosX() + 1, i.getPosY() - 1, matriz);
							i.setListaAdyacente(i.getPosX() - 1, i.getPosY(), matriz);
							i.setListaAdyacente(i.getPosX() + 1, i.getPosY(), matriz);
							i.setListaAdyacente(i.getPosX() - 1, i.getPosY() + 1, matriz);
							i.setListaAdyacente(i.getPosX(), i.getPosY() + 1, matriz);
							i.setListaAdyacente(i.getPosX() + 1, i.getPosY() + 1, matriz);
						}
					}
				} else {
					if (i.getPosX() == 0) {
						i.setListaAdyacente(1, 0, matriz);
					} else if (i.getPosX() + 1 == m) {
						i.setListaAdyacente(m - 2, 0, matriz);
					} else {
						i.setListaAdyacente(i.getPosX() - 1, 0, matriz);
						i.setListaAdyacente(i.getPosX() + 1, 0, matriz);
					}
				}
			} else {
				if (n > 1) {
					if (i.getPosY() == 0) {
						i.setListaAdyacente(0, 1, matriz);
					} else if (i.getPosY() + 1 == n) {
						i.setListaAdyacente(0, n - 2, matriz);
					} else {
						i.setListaAdyacente(0, i.getPosY() - 1, matriz);
						i.setListaAdyacente(0, i.getPosY() + 1, matriz);
					}
				}
			}
		}
	}

	public void anadirNodo(Nodo nodo) {
		listaNodos.add(nodo);
	}

	public Nodo getNodo(int n, int a, int b) {
		int numNodo = obtenerNumeroNodo(n, a, b);
		Nodo nodo = listaNodos.get(numNodo);
		return nodo;
	}

	private int obtenerNumeroNodo(int n, int a, int b) {
		int numNodo = n * b + a;
		return numNodo;
	}

	private void anadirNodosLista(int n, int m, Nodo[][] matriz) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				Nodo nodo = matriz[i][j];
				anadirNodo(nodo);
			}
		}
	}

	public List<Nodo> getListaNodos() {
		return listaNodos;
	}
}